﻿namespace Domain;
public class Produto
{
    public Guid Id { get; private set;}
    public string Nome { get; private set; } = "";
    public decimal Valor { get; private set; }

    public Produto(Guid id, string nome, decimal valor)
    {
        Id = id;
        Nome = nome;
        Valor = valor;
    }

    public void SetNome(string nome)
    {
        if(nome == null || nome.Length == 0)
        {
            throw new Exception("Nome não pode ser nulo ou vazio");
        }

        Nome = nome;
    }

    public bool SetValor(decimal valor)
    {
        if (valor <= 0)
        {
            return false;
        }

        Valor = valor;

        return true;
    }
}
