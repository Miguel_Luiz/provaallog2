﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Tests.Entities
{
    public class ProdutoTestsFixture
    {
        public Produto GenerateValidProduto()
        {
            return new Produto(
                Guid.NewGuid(),
               "Bombom",
               12.4m
                );
        }

        public Produto GenerateValidProdutoWithBogus()
        {
            return new Faker<Produto>().CustomInstantiator(f => new Produto(
                f.Random.Guid(),
               f.Random.AlphaNumeric(7),
               f.Random.Decimal(0, 20)
                ));
        }
    }
}
