using Xunit;

namespace Domain.Tests.Entities;

public class ProdutoTests : IClassFixture<ProdutoTestsFixture>
{
    private readonly ProdutoTestsFixture _fixture;

    public ProdutoTests(ProdutoTestsFixture fixture)
    {
        _fixture = fixture;
    }

    [Fact(DisplayName = "Teste do nome do produto quando nulo")]
    public void Validate_ProductNameIsNull_ShouldReturnException()
    {
        var produto = _fixture.GenerateValidProduto();

        var exception = Record.Exception(() => produto.SetNome(null));

        Assert.IsType<Exception>(exception);
        Assert.Equivalent("Nome n�o pode ser nulo ou vazio", exception.Message);
    }

    [Fact(DisplayName = "Teste do valor do produto quando menor ou igual a zero")]
    public void Validate_ProductValorIsZeroOrLess_ShouldReturnException()
    {
        var produto = _fixture.GenerateValidProdutoWithBogus();

        var result = produto.SetValor(-2);

        Assert.False(result);
    }
}